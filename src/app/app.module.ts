import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppComponent } from './app.component';

import { AppRoutingModule } from './app-routing.module';
import {CommonModule} from "@angular/common";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {HttpClientModule} from "@angular/common/http";
import {DocFlowService} from "./core/services/doc-flow.service";
import {PrimeNGModule} from "./prime-ng.module";

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    PrimeNGModule,
    CommonModule,
    HttpClientModule,
    AppRoutingModule
  ],
  providers: [DocFlowService],
  bootstrap: [AppComponent]
})
export class AppModule { }
