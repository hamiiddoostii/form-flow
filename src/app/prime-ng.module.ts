import { NgModule } from '@angular/core';
import {StepsModule} from "primeng/steps";
import {ToastModule} from "primeng/toast";
import {CardModule} from "primeng/card";
import {ButtonModule} from "primeng/button";
import {FileUploadModule} from "primeng/fileupload";
import {InputNumberModule} from "primeng/inputnumber";
import {CalendarModule} from "primeng/calendar";
import {DropdownModule} from "primeng/dropdown";
import {TableModule} from "primeng/table";
import {PanelModule} from "primeng/panel";
import {DividerModule} from "primeng/divider";

@NgModule({
  imports: [
    StepsModule,
    ToastModule,
    CardModule,
    ButtonModule,
    InputNumberModule,
    CalendarModule,
    DropdownModule,
    TableModule,
    PanelModule,
    DividerModule
  ],
  exports: [
    StepsModule,
    ToastModule,
    FileUploadModule,
    CardModule,
    ButtonModule,
    InputNumberModule,
    CalendarModule,
    DropdownModule,
    TableModule,
    PanelModule,
    DividerModule
  ]
})
export class PrimeNGModule { }
