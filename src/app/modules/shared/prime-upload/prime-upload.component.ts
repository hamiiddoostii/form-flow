import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {DocFlowService} from "../../../core/services/doc-flow.service";

@Component({
  selector: 'app-prime-upload',
  templateUrl: './prime-upload.component.html',
  styleUrls: ['./prime-upload.component.css']
})
export class PrimeUploadComponent implements OnInit {

  constructor() {
  }

  @Input('multiple') multiple: boolean;
  @Input('accept') accept: string;
  @Input('image') image: string;
  @Input('showUploadButton') showUploadButton: boolean = false;
  @Input('showCancelButton') showCancelButton: boolean = false;
  @Output() uploadFiles: EventEmitter<any> = new EventEmitter<any>();

  ngOnInit(): void {
  }

  onUpload(event: any) {
    this.uploadFiles.emit(event.currentFiles);
  }

}
