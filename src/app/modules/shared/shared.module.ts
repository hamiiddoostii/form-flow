import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PrimeStepsComponent } from './prime-steps/prime-steps.component';
import { PrimeUploadComponent } from './prime-upload/prime-upload.component';
import { PrimeNGModule } from "../../prime-ng.module";
import { FormsModule } from "@angular/forms";


@NgModule({
  declarations: [
    PrimeStepsComponent,
    PrimeUploadComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    PrimeNGModule
  ],
  exports: [
    PrimeStepsComponent,
    FormsModule,
    PrimeUploadComponent,
    PrimeNGModule
  ]
})
export class SharedModule { }
