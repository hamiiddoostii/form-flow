import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PrimeStepsComponent } from './prime-steps.component';

describe('PrimeStepsComponent', () => {
  let component: PrimeStepsComponent;
  let fixture: ComponentFixture<PrimeStepsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PrimeStepsComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(PrimeStepsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
