import {Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges} from '@angular/core';
import {MenuItem} from "primeng/api";
import {Router} from "@angular/router";


@Component({
  selector: 'app-prime-steps',
  templateUrl: './prime-steps.component.html',
  styleUrls: ['./prime-steps.component.css']
})
export class PrimeStepsComponent implements OnInit {

  constructor(private router: Router) {}

  @Input('items') items: MenuItem[];
  @Input('activeIndex') activeIndex: number;

  currentIndex: number;
  showNextBtn: boolean = true;
  showPrevBtn: boolean = true;

  ngOnInit() {
    this.router.navigate(["/" + this.items[this.activeIndex].routerLink[0]])
    if (this.activeIndex == 0) {
      this.showPrevBtn = false;
    }
  }

  activeIndexChange(event: number) {
    this.activeIndex = event;
    this.checkBtnShow();
  }

  checkBtnShow() {
    this.showPrevBtn = this.activeIndex != 0;
    this.showNextBtn = this.activeIndex + 1 != this.items.length;
  }

  nextPage() {
    this.router.navigate(["/" + this.items[this.activeIndex + 1].routerLink[0]]);
    this.activeIndex += 1;
    this.checkBtnShow();
  }

  prevPage() {
    this.router.navigate(["/" + this.items[this.activeIndex - 1].routerLink[0]]);
    this.activeIndex -= 1;
    this.checkBtnShow();
  }

}
