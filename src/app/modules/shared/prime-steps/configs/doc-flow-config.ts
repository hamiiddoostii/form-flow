import {MenuItem} from "primeng/api";

export class DocFlowConfig {

  stepsList: MenuItem[] = [
    {
      label: 'imageUpload',
      routerLink: ["imageUpload"],
      command: (event: any) => {
        // console.log(event)
      }
    },
    {
      label: 'docForm',
      routerLink: ["docForm"],
      command: (event: any) => {
        // console.log(event)
      }
    },
    {
      label: 'people',
      routerLink: ["people"],
      command: (event: any) => {
        // console.log(event)
      }
    },
    {
      label: 'review',
      routerLink: ["review"],
      command: (event: any) => {
        // console.log(event)
      }
    }
  ]
}
