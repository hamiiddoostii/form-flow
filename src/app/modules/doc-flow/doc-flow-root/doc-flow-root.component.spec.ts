import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DocFlowRootComponent } from './doc-flow-root.component';

describe('DocFlowRootComponent', () => {
  let component: DocFlowRootComponent;
  let fixture: ComponentFixture<DocFlowRootComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DocFlowRootComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(DocFlowRootComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
