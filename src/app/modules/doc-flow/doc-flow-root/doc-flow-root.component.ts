import {Component, DoCheck, OnInit} from '@angular/core';
import {DocFlowConfig} from "../../shared/prime-steps/configs/doc-flow-config";
import {MenuItem} from "primeng/api";
import {DocFlowService} from "../../../core/services/doc-flow.service";

@Component({
  selector: 'app-doc-flow-root',
  templateUrl: './doc-flow-root.component.html',
  styleUrls: ['./doc-flow-root.component.css']
})
export class DocFlowRootComponent implements OnInit {

  constructor() { }

  itemList: MenuItem[];
  activeIndex: number = 0;

  ngOnInit(): void {
    this.itemList = new DocFlowConfig().stepsList;
  }

}
