import { Component, OnInit } from '@angular/core';
import {DocFlowService} from "../../../core/services/doc-flow.service";

@Component({
  selector: 'app-review',
  templateUrl: './review.component.html',
  styleUrls: ['./review.component.css']
})
export class ReviewComponent implements OnInit {

  constructor(private docFlowService: DocFlowService) { }

  infoData: any;

  ngOnInit(): void {
    this.infoData = this.docFlowService.getInformation();
    console.log(this.infoData)
  }

}
