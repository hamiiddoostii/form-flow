import { Component, OnInit } from '@angular/core';
import {DocFlowService} from "../../../core/services/doc-flow.service";

@Component({
  selector: 'app-image-upload',
  templateUrl: './image-upload.component.html',
  styleUrls: ['./image-upload.component.css']
})
export class ImageUploadComponent implements OnInit {

  constructor(public docFlowService: DocFlowService) { }

  image: string;

  ngOnInit(): void {
    if (this.docFlowService.personalInfo.image) {
      this.image = this.docFlowService.personalInfo.image;
    }
  }

  onUpload(event: any) {
    this.docFlowService.personalInfo.image = event[0].objectURL.changingThisBreaksApplicationSecurity;
  }

}
