import {Component, OnDestroy, OnInit} from '@angular/core';
import {Router} from "@angular/router";
import {DocFlowService} from "../../../core/services/doc-flow.service";

@Component({
  selector: 'app-doc-form',
  templateUrl: './doc-form.component.html',
  styleUrls: ['./doc-form.component.css']
})
export class DocFormComponent implements OnInit, OnDestroy {

  constructor(private docFlowService: DocFlowService) { }

  personalInformation = {
    amount: null,
    date: null,
    city: "",
    letter: ""
  };
  cities = [
    { name: "Alborz"},
    { name: "Esfehan"},
    { name: "Tehran"},
    { name: "Tabriz"}
  ]
  submitted: boolean = false;
  fromDateMax: Date;

  ngOnInit(): void {
    this.setValidDate();
    this.setInitData();
  }

  setInitData() {
    this.personalInformation.amount = this.docFlowService.personalInfo.amount;
    this.personalInformation.date = this.docFlowService.personalInfo.date;
    this.personalInformation.city = this.docFlowService.personalInfo.city;
    this.personalInformation.letter = this.docFlowService.personalInfo.letter;
  }

  setValidDate() {
    this.fromDateMax = new Date(Date.now() + 432000000);
  }

  onUpload(event: any) {
    this.personalInformation.letter = event[0];
  }

  setData() {
    this.docFlowService.personalInfo.amount = this.personalInformation.amount;
    this.docFlowService.personalInfo.date = this.personalInformation.date;
    this.docFlowService.personalInfo.city = this.personalInformation.city;
    this.docFlowService.personalInfo.letter = this.personalInformation.letter;
  }

  ngOnDestroy(): void {
    this.setData()
  }

}
