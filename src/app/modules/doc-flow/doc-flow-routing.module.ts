import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {DocFlowRootComponent} from "./doc-flow-root/doc-flow-root.component";
import {ImageUploadComponent} from "./image-upload/image-upload.component";
import {DocFormComponent} from "./doc-form/doc-form.component";
import {PeopleListComponent} from "./people-list/people-list.component";
import {ReviewComponent} from "./review/review.component";

const routes: Routes = [
  {
    path: '', component: DocFlowRootComponent, children: [
      {path: 'imageUpload', component: ImageUploadComponent},
      {path: 'docForm', component: DocFormComponent},
      {path: 'people', component: PeopleListComponent},
      {path: 'review', component: ReviewComponent}
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DocFlowRoutingModule {
}
