import {Component, OnDestroy, OnInit} from '@angular/core';
import {DocFlowService} from "../../../core/services/doc-flow.service";

@Component({
  selector: 'app-people-list',
  templateUrl: './people-list.component.html',
  styleUrls: ['./people-list.component.css']
})
export class PeopleListComponent implements OnInit, OnDestroy {

  constructor(private docFlowService: DocFlowService) { }

  persons = [
    { code: 1, name: 'Ali', education: 'Primary' },
    { code: 2, name: 'Hamed', education: 'Lower secondary' },
    { code: 3, name: 'Sara', education: 'Upper secondary' },
    { code: 4, name: 'Amir', education: 'Post-secondary' },
    { code: 5, name: 'Nima', education: 'bachelors' }
  ]

  selectedPerson: any;

  ngOnInit(): void {
    this.setInitData();
  }

  setInitData() {
    this.selectedPerson = this.docFlowService.personalInfo.selectedPerson;
  }

  setData() {
    this.docFlowService.personalInfo.selectedPerson = this.selectedPerson;
  }

  ngOnDestroy(): void {
    this.setData()
  }

}
