import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DocFlowRoutingModule } from './doc-flow-routing.module';
import { ImageUploadComponent } from './image-upload/image-upload.component';
import { DocFormComponent } from './doc-form/doc-form.component';
import { PeopleListComponent } from './people-list/people-list.component';
import { ReviewComponent } from './review/review.component';
import { DocFlowRootComponent } from './doc-flow-root/doc-flow-root.component';
import {SharedModule} from "../shared/shared.module";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";

@NgModule({
  declarations: [
    ImageUploadComponent,
    DocFormComponent,
    PeopleListComponent,
    ReviewComponent,
    DocFlowRootComponent
  ],
    imports: [
        CommonModule,
        FormsModule,
        DocFlowRoutingModule,
        SharedModule
    ]
})
export class DocFlowModule { }
