import { TestBed } from '@angular/core/testing';

import { DocFlowService } from './doc-flow.service';

describe('DocFlowService', () => {
  let service: DocFlowService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(DocFlowService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
