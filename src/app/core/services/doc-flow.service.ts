import { Injectable } from '@angular/core';
import {Subject} from "rxjs";


@Injectable({
  providedIn: 'root'
})
export class DocFlowService {

  personalInfo = {
    image: "",
    amount: null,
    date: null,
    city: "",
    letter: "",
    selectedPerson: null
  }

  getInformation() {
    return this.personalInfo;
  }

}
